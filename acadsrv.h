#ifndef ACADSRV_H
#define ACADSRV_H

#include "Acad.h"
#include "Reg.h"
#include <vector>
#include <string>

using namespace std;

namespace Acad
{


    class AcadSrv
    {
    public:
        AcadSrv();

        ~AcadSrv();

        static vector<Acad::AcadApp>  GetInstalledAcad();

        static wstring GetAcadFullName(Acad::AcadApp *acd);

        static void HideAcadWindow(_PROCESS_INFORMATION *pid, int timeOut);

        static void HideAcadWindow(wstring name, _PROCESS_INFORMATION *pid);

        static HANDLE StartApplication(wstring name, wstring params, bool hidden);

        static int WaitForAppFinished(HANDLE handle, int timeOut);

        static vector<pair<wstring, unsigned long>> SaveAcadParams();

    private:
        static BOOL CALLBACK SEnumWindowsProc(HWND handle,LPARAM lParam); // st
        BOOL EnumWindowsProc(HWND handle,LPARAM lParam);

    };
}

#endif // ACADSRV_H
