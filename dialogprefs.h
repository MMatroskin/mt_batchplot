#ifndef DIALOGPREFS_H
#define DIALOGPREFS_H

#include <QWidget>
#include <QDialog>
#include <QString>
#include <QList>
#include <QDir>
#include <QStringListModel>
#include <QStandardItemModel>
#include <string>
#include <vector>
#include "Acad.h"
#include "comboboxdelegate.h"
#include "ui_dialogprefs.h"

namespace Ui {

struct State{
    std::wstring acad;
    std::wstring profile;
    std::wstring pdfPlotter;
};

class DialogPrefs;
}

class DialogPrefs : public QDialog, public Ui_DialogPrefs
{
    Q_OBJECT

public:
    explicit DialogPrefs(QWidget *parent = nullptr);
    ~DialogPrefs();

    vector<Acad::AcadApp>  acadList;
    std::wstring appPath;
    std::wstring printersDescDir;
    std::wstring iniFileNameSizes;
    std::wstring iniFileNameMain;
    Ui::State currentState;

    void SetAcadListData();
    void ChangeButtonCancelText(bool onStart);

protected:
    Ui::State result;

signals:
    void ReturnRes(Ui::State result);

private slots:
    void on_pushButtonOk_clicked();
    void on_comboBoxAcad_activated(int index);
    void on_comboBoxProfile_activated(int index);
    void on_currentTabChanged(int index);

private:
    Ui::DialogPrefs *ui;
    QStringListModel *modelAcadNamesList;
    QStringListModel *modelProfilesNamesList;
    QStringListModel *modelPrintersNamesList;
    QStandardItemModel *modelPapers;
    Acad::AcadApp  *acd;
    QStringList acadNamesList;
    QStringList acadProfilesList;
    QStringList systemPrintersList;
    QStringList papersNamesList;
    vector<pair<wstring, map<wstring, wstring>>> params;

    QStringList GetProfilesList(Acad::AcadApp  *app);
    QStringList GetPrintersNamesList(Acad::AcadApp *acd, wstring profile);

    void SetOkEnabled(bool enable);
    void SetAcadNamesList(QStringList acadNamesList);
    void SetComboBoxAcadListData();
    void SetProfilesNamesList(QStringList profileNamesList);
    void SetPrintersNamesList(QStringList printersNamesList);
    void ChangeModels();
    vector<pair<wstring, map<wstring, wstring>>> GetPrefs(wstring fileName);
    bool SavePrefs();

};

#endif // DIALOGPREFS_H
