#ifndef REG_H
#define REG_H

#include <windows.h>
#include <vector>
#include <string>

using namespace std;

namespace Reg
{
	class RegFunc
	{
	public:
		RegFunc();
		~RegFunc();

        static wstring GetParametr(
			HKEY key,
            wstring subKey,
            wstring name);

        static unsigned long GetDwParametr(
            HKEY key,
            wstring subKey,
            wstring name);

        static vector<wstring> GetSubKeys(HKEY key,
            wstring subKey);

        static bool SetParametr(
            HKEY rKey,
            wstring subKey,
            wstring name,
            wstring value);

        static bool SetParametr(
            HKEY rKey,
            wstring subKey,
            wstring name,
            unsigned long value);

        static bool CreateItem(HKEY rKey,
            wstring subKey);

	};
}

#endif
