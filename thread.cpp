#include "thread.h"
#include "Acad.h"
#include "acadsrv.h"
#include "FilesQt.h"
#include "Files.h"
#include "mainwindow.h"
#include <windows.h>

namespace Ui{
    Thread::Thread(){

    }

    Thread::~Thread(){

    }

    void Thread::SetArg(QString str,
                        QStringList fileNameListIn,
                        QString appNameIn,
                        QString profileIn,
                        QString supportPath,
                        int timeOut,
                        bool isSilent){
        this->scriptFileName = str;
        this->fileNameList = fileNameListIn;
        this->appName = appNameIn;
        this->profile = profileIn;
        this->supportPath = supportPath;
        this->timeOut = timeOut;
        this->isSilent = isSilent;
    }

    void Thread::run(){

        int appResult = 0;
        running = true;
        count = 0;

//        appName = FileQts::FilesQt::AddQuotes(appName);
        // для StartProcess  не нужны внутренние кавычки
        // (в отличие от QProcess)

        scriptFileName = Files::FilesQt::ChangeSlash(scriptFileName);
        scriptFileName = Files::FilesQt::AddQuotes(scriptFileName);
        //supportPath = FilesQt::FilesQt::AddQuotes(supportPath);

        // печать всех файлов из списка fileNameList

        while (!fileNameList.empty() && (running == true)){
            int result = 1;
            QString fileName = fileNameList.front();
            fileNameList.pop_front();
            fileName = Files::FilesQt::ChangeSlash(fileName);
            fileName = Files::FilesQt::AddQuotes(fileName);

            // строка для запуска процесса через WinApi
            QString params = Files::FilesQt::AddSlash(fileName) +
                    " /product ACAD /nologo /nossm /p \"" +
                    profile +
                    "\" /b  " +
                    Files::FilesQt::AddSlash(scriptFileName);
            if (supportPath != ""){
                params = params +
//                        "\"" +
                        " /s \"" +
//                        FilesQt::FilesQt::AddSlash(supportPath)
                        supportPath +
                        "\""; // for WinApi
            }

//            params = params + "\""; // for WinApi

            wstring wsCmdLine = params.toStdWString();
            wstring wsAppName = appName.toStdWString();

            HANDLE handle = Acad::AcadSrv::StartApplication(
                        wsAppName,
                        wsCmdLine,
                        this->isSilent // true/false  - hide/show window
                        );
            if(handle != NULL){
                result = Acad::AcadSrv::WaitForAppFinished(handle, timeOut);
//                if (result == 1){
//                    emit reportItemResult(result);
//                    break;
//                }
            }
//            else{
//                emit reportItemResult(result);
//                break;
//            }
            if(result == 1){
                appResult = 1;
            }

            count++;
            emit progress(count);
            emit reportItemResult(result);
        }
        emit finish(count); // finish
        emit reportResult(appResult);
    }

    void Thread::Canceled(){

        running = false;
    }

}
