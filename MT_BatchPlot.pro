#-------------------------------------------------
#
# Project created by QtCreator 2019-07-26T15:37:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = MT_BatchPlot
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    comboboxdelegate.cpp \
    configSrv.cpp \
    dialogprefs.cpp \
    filesACD.cpp \
    filesQt.cpp \
        main.cpp \
        mainwindow.cpp \
    Acad.cpp \
    Reg.cpp \
    acadsrv.cpp \
    files.cpp \
    countwindows.cpp \
    thread.cpp \
    logsrv.cpp \
    dialoghistory.cpp

HEADERS += \
    comboboxdelegate.h \
    configSrv.h \
    dialogprefs.h \
    filesACD.h \
    filesQt.h \
        mainwindow.h \
    Acad.h \
    Reg.h \
    acadsrv.h \
    files.h \
    countwindows.h \
    thread.h \
    logsrv.h \
    dialoghistory.h

FORMS += \
        dialogprefs.ui \
        mainwindow.ui \
    dialoghistory.ui


