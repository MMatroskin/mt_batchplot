#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "files.h"
#include "filesQt.h"
#include "filesACD.h"
#include "acadsrv.h"
#include "logsrv.h"
#include "dialoghistory.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QProgressDialog>
#include <QThread>
#include <QTime>
#include <QDate>
#include <QPalette>
#include <windows.h>

MainWindow::MainWindow(QWidget *parent, bool isCalling) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    this->isCalling = isCalling;
    this->enabled = true;

    registry = new Reg::RegFunc();
    acd = new Acad::AcadApp();
    modelFileList = new QStringListModel(this);
    modelPlotStylesList = new QStringListModel(this);

    connect(ui->listView, &QListView::clicked, this, &MainWindow::on_fileList_activated);
    connect(ui->pushButtonCancel, &QPushButton::clicked, this, &MainWindow::SlotExit);
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::SlotExit);

    ui->listView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->listView, &QListView::customContextMenuRequested, this, &MainWindow::SlotCustomMenuRequested);
    connect(ui->lineEditPdf, &QLineEdit::textChanged, this, &MainWindow::SlotCheckDir);

    // inital state
    paramsState = {false, false};

}

MainWindow::~MainWindow()
{

//    delete registry;
//    delete acd;
//    delete modelFileList;
//    delete modelPlotStylesList;
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event){

    int key = event->key();
    if(key == Qt::Key_Delete){
        if(event->modifiers() & Qt::ShiftModifier){
            SlotRemoveAllRecords();
        }else {
            SlotRemoveRecords();
        }
    }else if(key == Qt::Key_Plus && event->modifiers() & Qt::CTRL){
        on_pushButtonFile_clicked();
    }

}

void MainWindow::SlotCustomMenuRequested(QPoint pos){

    QMenu * menu = new QMenu(this);
    QAction * addItems = new QAction("Добавить файлы (Ctrl+Plus)", this);
    QAction * removeItems = new QAction("Удалить из списка (Del)", this);
    QAction * removeAll = new QAction("Очистить список (Shift+Del)", this);

    connect(addItems, SIGNAL(triggered()), this, SLOT(on_pushButtonFile_clicked()));
    connect(removeItems, SIGNAL(triggered()), this, SLOT(SlotRemoveRecords()));
    connect(removeAll, SIGNAL(triggered()), this, SLOT(SlotRemoveAllRecords()));

    menu->addAction(addItems);
    menu->addAction(removeItems);
    menu->addAction(removeAll);

    menu->popup(ui->listView->viewport()->mapToGlobal(pos));
}

void MainWindow::SlotRemoveRecords(){

    for(int i = 0; i < fileSelectedList.size(); i++){
        int idx = fileList.indexOf(fileSelectedList[i]);
        if(idx >= 0 && idx < fileList.size()){
            fileList.removeAt(idx);
        }
    }
    fileSelectedList.clear();
    SetModelFileList(fileList, false);
}

void MainWindow::SlotRemoveAllRecords(){

    fileSelectedList.clear();
    fileList.clear();
    SetModelFileList(fileList, false);
}

void MainWindow::SlotCheckDir(){

    QString dirLineVal = ui->lineEditPdf->text();
    wstring dirName = Files::Files::ChangeSlash(dirLineVal.toStdWString());
    dirName = Files::Files::ChangeSlash(dirName);
    wstring tmp = &dirName.back();
    while(tmp == L"\\"){
        dirName.pop_back();
        tmp = &dirName.back();
    }
    if(this->pdfPath != dirName){
        bool isPdfDirExist = Files::Files::CheckDir(dirName);
        bool isPdfDirEmpty = CheckOutDir(dirName);
        if(isPdfDirExist && !isPdfDirEmpty){
            // show warning
            QString message = "Каталог\n" + dirLineVal + "\nсодержит PDF файлы.\nПри печати файлы будут перезаписаны.";
            QMessageBox msgBox;
            msgBox.setText(message);
            msgBox.setWindowTitle("Warning!");

            msgBox.exec();
        }

        this->pdfPath = dirName;
    }
}

void MainWindow::SlotExit(){

    this->close();
}

void MainWindow::on_actionAbout_triggered(){

    QDate currentDate = QDate::currentDate();
    int currentYear = currentDate.year();
    int startYear = 2019;
    QString tmp = QString::number(startYear);
    if(currentYear > startYear){
        tmp = tmp + " - " + QString::number(currentYear);
    }
    QString str = "Batch plotting utility for AutoCAD.\n(c) Maksim Tulin " + tmp + "\nnovymir-vlg@yandex.ru \nhttps://t.me/MMatroskin";
    QMessageBox::about(nullptr, "About", str);
}

void MainWindow::on_actionAboutQT_triggered(){

    QMessageBox::aboutQt(this, "About Qt");
}

void MainWindow::on_fileList_activated(){

    fileSelectedList.clear();
    QModelIndexList indexList = ui->listView->selectionModel()->selectedIndexes();
    for (int i = 0; i < indexList.count(); i++){
        int tmp = indexList.at(i).row();
        fileSelectedList.append(fileList[tmp]);
    }
}

void MainWindow::on_actionPreferences_triggered(bool onStart){

    Ui::State oldState = currentState;
    DialogPrefs *dlgPlotPref = new DialogPrefs(this);
    dlgPlotPref->setModal(true);

    connect(dlgPlotPref, &DialogPrefs::ReturnRes, this, &MainWindow::SetPrefs);

    dlgPlotPref->appPath = appPath;
    dlgPlotPref->iniFileNameSizes = appPath + L"\\Support\\MT_papersizes.ini";
    dlgPlotPref->iniFileNameMain = appPath + L"\\Support\\MT_batchplot.ini";
    dlgPlotPref->printersDescDir = printersDescPath;
    dlgPlotPref->currentState.pdfPlotter = currentState.pdfPlotter;
    dlgPlotPref->currentState.acad = currentState.acad;
    dlgPlotPref->currentState.profile = currentState.profile;
    dlgPlotPref->acadList = acadList;
    dlgPlotPref->SetAcadListData();
    dlgPlotPref->ChangeButtonCancelText(onStart);
    int dialogCode = dlgPlotPref->exec();

    delete dlgPlotPref;

    if(onStart && dialogCode == 0){
        this->enabled = false;
    }else {
        bool cmp = CompareState(oldState, currentState);
        if (dialogCode == 1 && !cmp){
            this->printEnable = false;
            vector<wstring> keyList = {
                L"ACAD",
                L"TemplatePath"
            };
            this->currentProfilePropertyesList =
                        acd->GetProfileProperties(currentState.profile, keyList);
            ClearPrintersDescDir();
            CreatePrintersDesc();
        }
    }
}

void MainWindow::on_actionHistory_triggered(){

    QString msg;
    list<wstring> lineList = Files::Files::GetFileLinesList(taskLogFile);
    list<wstring>::iterator it;
    for(it = lineList.begin(); it != lineList.end(); it++){
        wstring tmp = it->data();
        msg = msg + "\n" + QString::fromStdWString(tmp);
    }

    DialogHistory *dlgHistory = new DialogHistory(this);
    dlgHistory->setModal(true);
    dlgHistory->setAttribute(Qt::WA_DeleteOnClose);
    dlgHistory->setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);
    dlgHistory->setWindowTitle("История");
    dlgHistory->ShowData(msg);
    dlgHistory->show();
//    dlgHistory->raise();
//    dlgHistory->activateWindow();

}

void MainWindow::on_pushButtonFile_clicked(){

    QString workPath = QString::fromStdWString(lastDwgPath);
    if(workPath == ""){
        workPath = QDir::homePath();
    }

//    // test:
//    QFileDialog *dialog = new QFileDialog(this);
//    dialog->setNameFilter("*.dwg");
//    dialog->setViewMode(QFileDialog::Detail);
//    dialog->setFileMode(QFileDialog::ExistingFiles);
//    dialog->setOption(QFileDialog::DontUseNativeDialog);
//    dialog->setDirectory(workPath);
//    dialog->show();
//    int dEx = dialog->exec();
//    QStringList inputFilesList = dialog->selectedFiles();

//    QStringList inputFilesList =
//            QFileDialog::getOpenFileNames(
//                this,
//                nullptr,
//                workPath,
//                tr("*.dwg")); // exeption in first call
//                              // (missing network drives(?) in native dialog)

    QStringList inputFilesList =
            QFileDialog::getOpenFileNames(
                this,
                nullptr,
                workPath,
                tr("*.dwg"),
                nullptr,
                QFileDialog::ReadOnly |
                QFileDialog::DontUseCustomDirectoryIcons |
                QFileDialog::DontResolveSymlinks |
                QFileDialog::DontUseNativeDialog);

    if(inputFilesList.isEmpty() == false){
        QString tmp = inputFilesList[0];
        int index = tmp.lastIndexOf("/");
        tmp.truncate(index);
        lastDwgPath = tmp.toStdWString(); // Files::ChangeSlash(tmp.toStdWString()) - ???
        SetModelFileList(inputFilesList, true);
    }
}

void MainWindow::on_pushButtonPdfPath_clicked(){

    QString startPath = QString::fromStdWString(lastPdfPath);
    if(startPath == ""){
        startPath = QDir::homePath();
    }

    QFileDialog *fileDialog = new QFileDialog(this);
    fileDialog->setOption(QFileDialog::DontUseNativeDialog);
    fileDialog->setOption(QFileDialog::ShowDirsOnly);
    fileDialog->setOption(QFileDialog::DontResolveSymlinks);
    fileDialog->setFileMode(QFileDialog::DirectoryOnly);

//    QGridLayout* mainLayout = qobject_cast<QGridLayout*>(fileDialog->layout());
//    QCheckBox *checkBoxSubDir = new QCheckBox("Создавать папку по разделу проекта");

//    mainLayout->addWidget(checkBoxSubDir, 4, 0);
//    fileDialog->setLayout(mainLayout);

    fileDialog->setDirectory(startPath);

    fileDialog->show();
    int res = fileDialog->exec();

    QStringList userInput = fileDialog->selectedFiles();

    delete fileDialog;

    if(res == 1 && userInput.isEmpty() == false){
        QString tmp = userInput[0];
        this->lastPdfPath = tmp.toStdWString();
        ui->lineEditPdf->setText(tmp);
//        SlotCheckDir();
    }
}

void MainWindow::on_printButton_clicked(){

    list<wstring> logLineList;
    wstring logMsg = Files::LogSrv::GetLineUserDate();
    logLineList.push_back(logMsg);

    this->createSubDirsForPdf = ui->checkBoxPdfSubDirs->isChecked();
    this->reportList.clear();

    vector<wstring> keyList = {
        L"ACAD",
        L"TemplatePath"
    };
    this->currentProfilePropertyesList =
                acd->GetProfileProperties(currentState.profile, keyList);
//    acadSysvars.sysvarsListBakup =
//            acd->GetProfileSysvars(currentState.profile, acadSysvars.sysvarsDefaultList);

    if(printersDescExist == false){
        this->printEnable = false;
        CreatePrintersDesc(); // printEnable->true
    }
    // if create or found printers desc:
    if (printersDescExist){
        int ret = QMessageBox::Ok;
        SetACDParams(currentState.profile, acadSysvars.sysvarsList, currentProfilePropertyesList, true);
        if (paramsState.isSysvarsChanged == false){
            QString str = "Системные переменные не изменены!\nПродолжить?";
            QMessageBox *mBox = new QMessageBox(
                        QMessageBox::Warning,
                        "Warning!",
                        str,
                        QMessageBox::Ok | QMessageBox::Cancel
                        );
            mBox->setDefaultButton(QMessageBox::Ok);
            mBox->setEscapeButton(QMessageBox::Cancel);

            ret = mBox->exec();
            delete mBox;
        }
        if(paramsState.isSysvarsChanged == true || ret == QMessageBox::Ok){
            // save log and print
            if(ui->checkBoxPDF->isChecked()){
                wstring pdfSaveStr = pdfPath;
                bool isExist = Files::Files::CheckDir(pdfPath);
                if(pdfSaveStr == L"" || !isExist){
                    pdfSaveStr = L"Drawings path";
                }
                logMsg = L"Print to PDF; Saved: " + pdfSaveStr;
            }else{
                wstring cps = ui->spinBoxCopies->text().toStdWString();
                logMsg = L"Print " + cps + L" copies";
            }
            logLineList.push_back(logMsg);
            Files::Files::UpdateTextFile(taskLogFile, logLineList);
            PrintInACD(currentState);
            SetACDParams(currentState.profile, acadSysvars.sysvarsListBakup, currentProfilePropertyesList, false);
        }

    }else{
        QString message = "Не найдены описания принтеров!";
        QMessageBox msgBox;
        msgBox.setText(message);
        msgBox.setWindowTitle("Error!");

        msgBox.exec();

        // save log
        logMsg = L"Error! Printers description not found.";
        logLineList.push_back(logMsg);
        Files::Files::UpdateTextFile(taskLogFile, logLineList);
    }
    Files::Files::UpdateTextFile(taskLogFile, {L""});
}

void MainWindow::PrintButtonEnabled(){

    ui->printButton->setEnabled(modelFileList->rowCount() > 0);
}

void MainWindow::SetInitalState(){

    currentState = {
        this->acadName,
        this->profileName,
        this->pdfPlotterInit
    };

    acadSysvars.sysvarsDefaultList = {
        {L"ShowProxyGraphics", 1}, // PROXYSHOW
        {L"ShowProxyDialog", 1}, // PROXYNOTICE
        {L"ARXDemandLoad", 3} // DEMANDLOAD
    };
    acadSysvars.sysvarsList = {
        {L"ShowProxyGraphics", 1},
        {L"ShowProxyDialog", 0},
        {L"ARXDemandLoad", 2} // 0 - ???;
    };

    this->taskLogFile = appPath + L"\\" + Files::LogSrv::GetLogFileName(L"MT_batchplothistory");
    this->lastDwgPath = path;
    this->lastPdfPath = L"";
    this->printersDescPath = appPath + L"\\Support\\Printers";
    this->printEnable = true;

    QDir *plottersDir = new QDir(QString::fromStdWString(printersDescPath));
    this->printersDescExist = !plottersDir->isEmpty();
    delete plottersDir;

    if(acadName != L"" && profileName != L""){
        SetPrefs(this->currentState);
    }else{
        on_actionPreferences_triggered(true);
//        while(profileName == L""){
//            // set prefs
//            on_actionPreferences_triggered(true);
//        }
    }

    SetModelFileListOnStart();
    ui->lineEditPdf->setText(QString::fromStdWString(this->pdfPath));
    ui->checkBoxPdfSubDirs->setChecked(this->createSubDirsForPdf);
    ui->labelPdf->setVisible(false);
    ui->lineEditPdf->setVisible(false);
    ui->pushButtonPdfPath->setVisible(false);
    ui->checkBoxPdfSubDirs->setVisible(false);

}

void MainWindow::ClearPrintersDescDir(){

    QDir *plottersDir = new QDir(QString::fromStdWString(printersDescPath));
    if (plottersDir->isEmpty() == false){
        QStringList items = plottersDir->entryList();
        while(!items.isEmpty()){
            QString item = items.front();
            plottersDir->remove(item);
            items.pop_front();
        }
    }
    delete plottersDir;
    printersDescExist = false;
}

void MainWindow::SetPrefs(Ui::State value){

    bool cmp = CompareState(value, this->currentState);
    if(!cmp){
        this->currentState = value;
    }
    this->currentState.pdfPlotter = value.pdfPlotter;

    for(size_t i = 0; i < acadList.size(); i++){
        if(acadList[i].GetProductName() == value.acad){
            this->acd = &acadList[i];
        }
    }

    acadSysvars.sysvarsListBakup =
            acd->GetProfileSysvars(currentState.profile, acadSysvars.sysvarsDefaultList);

    ui->labelAcad->setText(QString::fromStdWString(this->currentState.acad));
    ui->labelProfile->setText(QString::fromStdWString(this->currentState.profile));
    SetComboBoxPlotStylesListData();
}

bool MainWindow::CompareState(Ui::State state1, Ui::State state2){

    if((state1.acad == state2.acad) && (state1.profile == state2.profile)){
        return true;
    }
    else{
        return false;
    }
}

void MainWindow::SetACDParams(wstring profile,
                  vector<pair<wstring, unsigned long>> sysvarsList,
                  vector<pair<wstring, wstring>> sysvarsStrList,
                  bool isBefore){

    bool sysvarsStrSuccess = true;
    bool isSecureModeChanged = paramsState.isSecureModeChanged;
    bool isSysvarsChanged = paramsState.isSysvarsChanged;

    if (isBefore){
        // SECURELOAD disable in ACAD 2014+
        if (acd->IsSecureModeOn(profile)){
            isSecureModeChanged = acd->SecureModeChange(profile);
        }
        paramsState.isSecureModeChanged = isSecureModeChanged;

    }else{
        // restore sysvar and profile
        if (isSecureModeChanged == true){
            bool res = acd->SecureModeChange(profile);
            if(res){
                isSecureModeChanged = false;
            }
        }
        paramsState.isSecureModeChanged = isSecureModeChanged;
        sysvarsStrSuccess = acd->SetProfileSysvars(profile, sysvarsStrList);
    }

    // setup sysvars
    bool sysvarsSuccess = acd->SetProfileSysvars(profile, sysvarsList); // проверить на кириллицу

    if(sysvarsSuccess && sysvarsStrSuccess){
        if(isSysvarsChanged == false ){
            isSysvarsChanged = true;
        }
        else {
            isSysvarsChanged = false;
        }
        paramsState.isSysvarsChanged = isSysvarsChanged;
    }
}

void MainWindow::CreatePrintersDesc(){
    // run AutoCAD and create printers desc files

//    bool result = false;

//    acadSysvars.sysvarsListBakup =
//            acd->GetProfileSysvars(currentState.profile, acadSysvars.sysvarsDefaultList);

    SetACDParams(currentState.profile, acadSysvars.sysvarsList, currentProfilePropertyesList, true);

    if (paramsState.isSysvarsChanged == false){
        QString message = "Системные переменные не измнены!";
        QMessageBox msgBox;
        msgBox.setText(message);
        msgBox.setWindowTitle("Error!");

        msgBox.exec();
    }else{
//        vector<pair<wstring, wstring>> profilePropertyesList =
//                acd->GetProfileProperties(currentState.profile, keyList);
        QString templateDirName;
        for(size_t i = 0; i < currentProfilePropertyesList.size(); i++){
            if(currentProfilePropertyesList[i].first == L"TemplatePath"){
                templateDirName = QString::fromStdWString(currentProfilePropertyesList[i].second);
            }
        };
        QDir *templateDir = new QDir(templateDirName);
        QStringList filters;
        filters.append("*.dwt");
        QStringList templateList = templateDir->entryList(filters, QDir::Files, QDir::Name);
//        templateList.sort(Qt::CaseSensitive);

        QString fileName = templateDir->absolutePath() + "/" + templateList.back();

        QString scriptFileName = QDir::tempPath() + "/" + "exportPrefsScript.scr";
        QString profile = QString::fromStdWString(currentState.profile);
        QStringList fileNameList = {fileName};

        bool scriptExist = Files::FilesACD::CreatePlottersDescScript(
                    scriptFileName.toStdWString(),
                    appPath); // filesACD ???
        if (scriptExist){
            // prolonged operation. Runing the ACAD app & Lisp programm in this
            // export printers desc
            QString supportPathApp = QString::fromStdWString(appPath) + "\\Support";
            int timeOutLocal = timeOut * timeMult;
            StartACDInThread(fileNameList,
                             scriptFileName,
                             profile,
                             supportPathApp,
                             "Создание файлов описаний принтеров.\nОперация может занять некоторое время.",
                             timeOutLocal,
                             isSilent);
            Files::FilesQt::DeleteFileNow(scriptFileName);
            // save log from reportList
        }else{
            QString message = "Ошибка создания скрипта!";
            QMessageBox msgBox;
            msgBox.setText(message);
            msgBox.setWindowTitle("Error!");

            msgBox.exec();
        }

        delete templateDir;
    }

    SetACDParams(currentState.profile, acadSysvars.sysvarsListBakup, currentProfilePropertyesList, false);

    QDir *plottersDir = new QDir(QString::fromStdWString(printersDescPath));
    printersDescExist = !plottersDir->isEmpty();
    delete plottersDir;

//    if(printEnable && printersDescExist){
//        result = true;
//    }
//    return result;
}

bool MainWindow::CheckOutDir(wstring dir){

    bool result = false;

    list<wstring> resultList = Files::Files::SearchFiles(dir, L"*.pdf", false);
    if(resultList.empty()){
        result = true;
    }

    return result;
}

void MainWindow::PrintInACD(Ui::State currentState){

    // create script and plotting
    QString plotStyle = ui->comboBoxPlotStyles->currentText();
    int copies = ui->spinBoxCopies->value();
    bool pdfPlot = ui->checkBoxPDF->checkState();
    wstring outPath = this->pdfPath;
    QString scriptFileName = QDir::tempPath() + "/" + "plotScript.scr"; // script in %tempdir%
    bool plotScriptSuccess = Files::FilesACD::CreatePlotScript(scriptFileName.toStdWString(),
                                                               appPath,
                                                               plotStyle.toStdWString(),
                                                               copies,
                                                               pdfPlot,
                                                               outPath); // create plotScript

    if(plotScriptSuccess == true){
        QString supportPathApp = QString::fromStdWString(appPath) + "\\Support";
        QStringList fileNameList = modelFileList->stringList();
        StartACDInThread(fileNameList,
                         scriptFileName,
                         QString::fromStdWString(currentState.profile),
                         supportPathApp,
                         "Печать",
                         timeOut,
                         isSilent); // printing
        bool isScriptDeleting = Files::FilesQt::DeleteFileNow(scriptFileName);// delete script
        if (isScriptDeleting == false){
            // dick with him
        }
        // save log from reportList
    }
    else{
        QString str = "Ошибка создания скрипта печати!";
        QMessageBox::critical(
                    nullptr,
                    "Error!",
                    str,
                    QMessageBox::Ok | QMessageBox::Escape
                    );
    }
}

void MainWindow::StartACDInThread(QStringList fileNameList,
                                  QString scriptFileName,
                                  QString profile,
                                  QString path,
                                  QString text,
                                  int timeOut,
                                  bool silent){

    wstring wsAppName = Acad::AcadSrv::GetAcadFullName(acd);
    QString appName = QString::fromStdWString(wsAppName);

    QString supportPath;
    for(size_t i = 0; i < currentProfilePropertyesList.size(); i++){
        if(currentProfilePropertyesList[i].first == L"ACAD"){
            supportPath = QString::fromStdWString(currentProfilePropertyesList[i].second);
        }
    };
    if(!path.isEmpty()){
        supportPath = path + ";" + supportPath;
    }

    QPalette plt = this->palette();
    plt.setColor(QPalette::Highlight, QColor(77, 166, 255, 255));

    count = 0;

    threadObject = new Ui::Thread;
    threadObject->SetArg(scriptFileName,
                         fileNameList,
                         appName,
                         profile,
                         supportPath,
                         timeOut,
                         silent);
    connect(this, &MainWindow::workCanceled, threadObject, &Ui::Thread::Canceled);
    connect(threadObject, &Ui::Thread::progress, this, &MainWindow::SetValue);
    connect(threadObject, &Ui::Thread::finish, this, &MainWindow::AppStop);
    connect(threadObject, &Ui::Thread::reportItemResult, this, &MainWindow::ItemReport);
    connect(threadObject, &Ui::Thread::reportResult, this, &MainWindow::AppReport);

    threadObject->start();
    appRuning = true;

    QProgressDialog *pprd = new QProgressDialog();
    pprd->setModal(true);
    pprd->setWindowFlags(Qt::WindowTitleHint | Qt::CustomizeWindowHint);
    pprd->setWindowTitle("Обработка задания");
    pprd->setLabelText(text);
    pprd->setMinimum(0);
    pprd->setMinimumDuration(0);
    pprd->setAutoReset(false);
    pprd->setAutoClose(true);
    pprd->setPalette(plt);

    int countMax;
    if (!printEnable){
        pprd->setCancelButton(0);
        QTime time;
        time.start();
        int i;
        pprd->setMaximum(timeOut);
        while (appRuning){
            i = time.elapsed();
            pprd->setValue(i);
            qApp->processEvents();
            if(pprd->wasCanceled()){
                emit workCanceled();
                break;
            }
        }
    }
    else{
        countMax = fileNameList.size();
        pprd->setMaximum(countMax);
        while(count < countMax){
            QString textTmp = " " + QString::number(count + 1) + " из " + QString::number(countMax) + ":\n";
            QString fileName = fileNameList[count]; // current drawing file
            this->currentFile = fileName.toStdWString();
//            std::pair<std::wstring, bool> report;
//            report.first = fileName.toStdWString();
//            report.second = false;
//            reportList.push_back(report);
            pprd->setLabelText(text + textTmp + fileName);
            pprd->setValue(count);
            qApp->processEvents();
            if(pprd->wasCanceled()){
                pprd->setWindowTitle("Отмена задания");
                emit workCanceled();
                break;
            }
        }
    }

    pprd->setValue(pprd->maximum());
    threadObject->wait();

    delete pprd;
    delete threadObject;

}

void MainWindow::StartAppInThread(QStringList fileNameList,
                                  QString scriptFileName,
                                  QString profile,
                                  QString path,
                                  QString text,
                                  int timeOut){

}

void MainWindow::SetModelFileList(QStringList newFileList, bool save){

    QStringList fileListTmp = newFileList;
    QString stringFiles = "Файлы для печати (" + QString::number(newFileList.size()) + "):";
    if(save){
        fileListTmp = fileList + fileListTmp;
    }
    this->fileList = fileListTmp;

    int dd = fileList.removeDuplicates();

    modelFileList->removeRows(0, modelFileList->rowCount());
    modelFileList->setStringList(fileList);

    ui->listView->setModel(modelFileList);
    ui->listView->setSelectionMode(QAbstractItemView::MultiSelection);
    ui->listView->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->labelFiles->setText(stringFiles);

    PrintButtonEnabled();
}

void MainWindow::SetModelFileListOnStart(){

    QStringList fileListStart;
    wstring dir;
    wstring mask = L"*.dwg";

    dir = path;
    if(dir != L""){
        std::list<std::wstring> fileListTmp = Files::Files::SearchFiles(dir, mask, true);
        for(list<wstring>::iterator it1 = fileListTmp.begin(); it1 != fileListTmp.end(); it1++){
            fileListStart.push_back(QString::fromStdWString(*it1));
        }
    }

    SetModelFileList(fileListStart, false);
}

QStringList MainWindow::GetFileList(QString dirPath, QString mask){

    QDir dir(dirPath);
    QStringList maskList = mask.split(" ");
    QStringList filesList = dir.entryList(maskList, QDir::Files);

    return filesList;
}


QStringList MainWindow::GetProfilesList(Acad::AcadApp  *app){

    QStringList acadProfilesList;
    vector<wstring> acadProfilesTmp = app->GetAcadProfiles();
    for(size_t i =0; i < acadProfilesTmp.size(); i++){
        acadProfilesList.push_back(QString::fromStdWString(acadProfilesTmp[i]));
    }

    return acadProfilesList;
}


QStringList MainWindow::GetPlotStylesList(Acad::AcadApp  *app, wstring profileName){

    vector<wstring> keyList = {
        L"PrinterStyleSheetDir"
    };
    QStringList plotStylesList;
    vector<pair<wstring, wstring>> profilePropertyesList =
            app->GetProfileProperties(profileName, keyList);
    for(size_t i = 0; i < profilePropertyesList.size(); i++){
        if (profilePropertyesList[i].first == L"PrinterStyleSheetDir"){

            // The Style Sheet Directory may be not unique !
            // Enumeration in a line with a separator ";"

            list<wstring> styleSheetDirs =
                    Files::Files::GetListFromString(profilePropertyesList[i].second, L";");
            list<wstring>::iterator it;
            for(it = styleSheetDirs.begin(); it != styleSheetDirs.end(); it++){
                QString printerStyleSheetDir = QString::fromStdWString(*it);
                plotStylesList = plotStylesList + GetFileList(printerStyleSheetDir, "*.ctb");
            }
        }
    }

    return plotStylesList;
}

void MainWindow::SetComboBoxPlotStylesListData(){

    QStringList plotStylesNamesList = GetPlotStylesList(acd, currentState.profile);
    modelPlotStylesList->setStringList(plotStylesNamesList);
    ui->comboBoxPlotStyles->setModel(modelPlotStylesList);
    ui->comboBoxPlotStyles->setCurrentIndex(0);

}

void MainWindow::SetValue(int value){

    count = value;
}

void MainWindow::AppStop(int value){

    if(printEnable){
        QStringList fileNameList = modelFileList->stringList();
        for(int i = 0; i < value; i++){
            fileNameList.pop_front();
        }
        SetModelFileList(fileNameList, false);
    }
    // if printing enabled delete current plotted file from the task list
}

void MainWindow::AppReport(int result){

    if(!printEnable){
        if(result == 0){
            printEnable = true;
        }
    }else{
        if(result != 0){
            ReportError();
        }
    }
    appRuning = false;
    // enable printing in case of successful export of printer parameters
}

void MainWindow::ItemReport(int value){

    list<wstring> logLineList;
    std::pair<std::wstring, bool> tmp;
    tmp.first = currentFile;
    if(value == 0){
        tmp.second = true;
    }else{
        tmp.second = false;
        logLineList.push_back(L"-- Error! Exit on Time Out.");
        Files::Files::UpdateTextFile(taskLogFile, logLineList);
    }
    reportList.push_back(tmp);
}

void MainWindow::ReportError(){

    QString msg = "Ошибка обработки заданий:";

    for(size_t i = 0; i != reportList.size(); i++) {
        wstring str;
        std::pair<std::wstring, bool> tmp;
        tmp = reportList[i];
        if(tmp.second == false){
            str = L"\n" + tmp.first;
            msg = msg + QString::fromStdWString(str);
        }
    }

    QMessageBox *mBox = new QMessageBox(
                QMessageBox::Information,
                "Warning!",
                msg,
                QMessageBox::Ok
                );
    mBox->setDefaultButton(QMessageBox::Ok);

    mBox->exec();
    delete mBox;
}
