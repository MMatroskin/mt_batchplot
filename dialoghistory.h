#ifndef DIALOGHISTORY_H
#define DIALOGHISTORY_H

#include <QDialog>
#include <QStringList>
#include "ui_dialoghistory.h"

namespace Ui {
class DialogHistory;
}

class DialogHistory : public QDialog, public Ui_DialogHistory
{
    Q_OBJECT

public:
    explicit DialogHistory(QWidget *parent = 0);
    ~DialogHistory();

    void ShowData(QString data);

    ;

private:
    Ui::DialogHistory *ui;
};

#endif // DIALOGHISTORY_H
