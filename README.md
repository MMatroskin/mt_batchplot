MT-BatchPlot

Batch printing App for AutoCAD 2010+.  Part of "MT-Project" for design office
The App call AutoCAD for printing. Using existing system and PC3 printers.

C++/QT & Visual Lisp.


App Directory structure:

Root			- parent App directory (may be absent)

-AppDir			- App directory

--"Support"		- Support files directory

---"Printers"	- Printers descriptions directory
