#include <QMessageBox>
#include <QPrinterInfo>
#include <map>
#include "dialogprefs.h"
#include "ui_dialogprefs.h"
#include "mainwindow.h"
#include "files.h"
#include "configSrv.h"
#include "logsrv.h"

DialogPrefs::DialogPrefs(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogPrefs)
{
    ui->setupUi(this);
    ui->pushButtonOk->setEnabled(false);

    modelAcadNamesList = new QStringListModel(this);
    modelProfilesNamesList = new QStringListModel(this);
    modelPrintersNamesList = new QStringListModel(this);
    modelPapers = new QStandardItemModel(this);

    modelPapers->setColumnCount(2);
    ui->tableView->setModel(modelPapers);

    ui->tabWidget->setCurrentWidget(ui->tabAcad);

    QList<QPrinterInfo> printers = QPrinterInfo::availablePrinters();
    while(!printers.isEmpty()){
        QString name = printers.front().printerName();
        printers.pop_front();
        systemPrintersList.append(name);
    }

    connect(ui->tabWidget, &QTabWidget::currentChanged, this, &DialogPrefs::on_currentTabChanged);
}

DialogPrefs::~DialogPrefs(){

//    delete modelAcadNamesList;
//    delete modelProfilesNamesList;
//    delete modelPrintersNamesList;
//    delete modelPapers;
    delete ui;
}

void DialogPrefs::SetAcadListData(){

//    QStringList acadNamesList;
    for(size_t i = 0; i < acadList.size(); i++){
        acadNamesList.push_back(QString::fromStdWString(acadList[i].GetProductName()));
    }
    modelAcadNamesList->setStringList(acadNamesList);

    SetComboBoxAcadListData();
}

QStringList DialogPrefs::GetProfilesList(Acad::AcadApp  *app){

    QStringList acadProfilesList;
    vector<wstring> acadProfilesTmp = app->GetAcadProfiles();
    for(size_t i = 0; i < acadProfilesTmp.size(); i++){
        acadProfilesList.push_back(QString::fromStdWString(acadProfilesTmp[i]));
    }

    return acadProfilesList;
}

void DialogPrefs::SetComboBoxAcadListData(){

    ui->comboBoxAcad->setModel(modelAcadNamesList);

    QString name = QString::fromStdWString(currentState.acad);
    int idx = acadNamesList.indexOf(name);
    if(idx < 0){
        idx = 0;
    }
    ui->comboBoxAcad->setCurrentIndex(idx);

    this->on_comboBoxAcad_activated(idx);

}

void DialogPrefs::on_comboBoxAcad_activated(int index){

    acd = &acadList[index];
    result.acad = acd->GetProductName();

    acadProfilesList = GetProfilesList(acd);

    modelProfilesNamesList->removeRows(0, modelProfilesNamesList->rowCount());
    modelProfilesNamesList->setStringList(acadProfilesList);
    ui->comboBoxProfile->setModel(modelProfilesNamesList);

    QString nameProfile = QString::fromStdWString(currentState.profile);
    QString nameAcad = QString::fromStdWString(currentState.acad);
    int idx = acadProfilesList.indexOf(nameProfile);
    wstring acadCurrent = ui->comboBoxAcad->currentText().toStdWString();
    if(idx < 0 || acadCurrent != currentState.acad){
        idx = 0;
    }
    ui->comboBoxProfile->setCurrentIndex(idx);
//    OkEnabled(false);

    this->on_comboBoxProfile_activated(idx);
}

void DialogPrefs::on_comboBoxProfile_activated(int index){

    QString profile = acadProfilesList[index];
    result.profile = profile.toStdWString();
    ChangeModels();
//    ui->tabWidget->setCurrentWidget(ui->tabPlotters);
}

void DialogPrefs::on_currentTabChanged(int index){

    bool enable = false;
    if(index == 1){
        enable = true;
    }
    SetOkEnabled(enable);
}

void DialogPrefs::SetOkEnabled(bool enable){

    ui->pushButtonOk->setEnabled(enable);
}

void DialogPrefs::ChangeButtonCancelText(bool onStart){

    if(onStart){
        ui->pushButtonCancel->setText("Выход");
    }
}

void DialogPrefs::ChangeModels(){

    params = GetPrefs(iniFileNameSizes);

    QStringList plottersList = GetPrintersNamesList(acd, result.profile);
    QStringList printersList = systemPrintersList + plottersList;
    printersList.push_front("Undefined");

    modelPrintersNamesList->setStringList(plottersList);
    modelPapers->clear();

    QStringList plottersCurrent;
    papersNamesList.clear();
    for(size_t i = 0; i < params.size(); i++){
        pair<wstring, map<wstring, wstring>> item = params[i];
        papersNamesList.push_back(QString::fromStdWString(item.first));
        map<wstring, wstring>::iterator it;
        it = item.second.find(L"printerName");
        if(it != item.second.end()){
            plottersCurrent.push_back(QString::fromStdWString(it->second));
        }
    }
    modelPapers->setVerticalHeaderLabels(papersNamesList);
    modelPapers->setHorizontalHeaderLabels(QList<QString>() << "Текущее устройство" << "Новое устройство");

    Ui::ComboBoxDelegate *delegate = new Ui::ComboBoxDelegate(ui->tableView);

    delegate->printers = printersList;
    delegate->printersCurrent = plottersCurrent;
//    delegate->setEditorData(new QComboBox(), QModelIndex());

    ui->tableView->setItemDelegateForColumn(1, delegate);
    ui->tableView->setColumnWidth(0, 190);
    ui->tableView->setColumnWidth(1, 240);

    int lim = modelPapers->rowCount();
    for (int i = 0; i < lim; ++i ) {
        QString name = plottersCurrent[i];
        QStandardItem *item0 = new QStandardItem(name);
        QModelIndex index = modelPapers->index(i, 1);
        item0->setEditable(false);
        modelPapers->setItem(i, 0, item0);
        modelPapers->setData(index, name, Qt::EditRole);
        delegate->setEditorData(new QComboBox(), modelPapers->index(i, 1));
        ui->tableView->openPersistentEditor(modelPapers->index(i, 1));
        int pos = index.data(Qt::DisplayRole).toInt();
        if(pos > 0){
            plottersCurrent[i] = printersList[pos];
        }
    }

    ui->comboBoxPdfPlotter->setModel(modelPrintersNamesList);
    int idx = plottersList.indexOf(QString::fromStdWString(currentState.pdfPlotter));
    if(idx >= 0){
        ui->comboBoxPdfPlotter->setCurrentIndex(idx);
    }

}

QStringList DialogPrefs::GetPrintersNamesList(Acad::AcadApp *acd, wstring profile){

    QStringList result;
    vector<wstring> tmp = acd->GetPlotters(profile);
    for(size_t i = 0; i < tmp.size(); i++){
        result.push_back(QString::fromStdWString(tmp[i]));
    }
    return result;
}

void DialogPrefs::on_pushButtonOk_clicked(){

    list<wstring> logLineList;
    wstring logFile = appPath + L"\\" + L"MT_batchplot.log";
    wstring logMsg = Files::LogSrv::GetLineUserDate() + L" Change settings";
    wstring logMsgStr = L"";

    result.acad = ui->comboBoxAcad->currentText().toStdWString();
    result.profile = ui->comboBoxProfile->currentText().toStdWString();
    result.pdfPlotter = ui->comboBoxPdfPlotter->currentText().toStdWString();
    bool isSaved = SavePrefs();

    if(!isSaved){
        logMsgStr = L" - Error!";
        QString str = "Ошибка сохранения настроек!";
        QMessageBox *mBox = new QMessageBox(
                    QMessageBox::Critical,
                    "Error!",
                    str,
                    QMessageBox::Ok
                    );
        mBox->setDefaultButton(QMessageBox::Ok);
        mBox->exec();
        delete mBox;
    }else{
        logMsgStr = L" - Success.";
        emit ReturnRes(result);
    }

    logMsg = logMsg + logMsgStr;
    logLineList.push_back(logMsg);
    Files::Files::UpdateTextFile(logFile, logLineList);

    this->close();
}

vector<pair<wstring, map<wstring, wstring>>> DialogPrefs::GetPrefs(wstring fileName){

    vector<pair<wstring, map<wstring, wstring>>> result;
    list<wstring> sections = Files::ConfigSrv::GetSections(fileName);
    list<wstring>::iterator it;
    for(it = sections.begin(); it != sections.end(); it++){
        wstring sectName = *it;
        map<wstring, wstring> params = Files::ConfigSrv::GetSectParams(fileName, sectName);
        result.push_back(pair<wstring, map<wstring, wstring>>(sectName, params));
    }

    return result;
}

bool DialogPrefs::SavePrefs(){

    bool result = true;

    QStringList printersList = systemPrintersList + GetPrintersNamesList(acd, this->result.profile);
    printersList.push_front("");
    map<wstring, wstring> userInput;
    int lim = modelPapers->rowCount();
    for(int i = 0; i < lim; i++){
//        QModelIndex index;
//        index = modelPapers->index(i, 0);
        wstring newVal = L"";
        wstring key = papersNamesList[i].toStdWString();
        QModelIndex index = modelPapers->index(i, 1);
        int pos = index.data(Qt::DisplayRole).toInt();
        if(pos > 0){
            newVal = printersList[pos].toStdWString();
            userInput.insert(pair<wstring, wstring> (key, newVal));
        }else if(pos == 0){
            for(size_t j = 0; j < params.size(); j++){
                if(params[j].first == key){
                    map<wstring, wstring> item = params[j].second;
                    map<wstring, wstring>::iterator it = item.find(L"printerName");
                    if(it != item.end()){
                        int idx = printersList.indexOf(QString::fromStdWString(it->second));
                        if(idx < 0){
                            userInput.insert(pair<wstring, wstring> (key, newVal));
                        }
                    }
                    break;
                }
            }
        } // if printer not found in system -> clear value !!
    }

    list<wstring> lineListSizes = Files::Files::GetFileLinesList(iniFileNameSizes);
    bool changeFile0 = false;
    for (size_t i = 0; i < params.size(); i++){
        wstring sectName = params[i].first;
        map<wstring, wstring>::iterator it0 = userInput.find(sectName);
        if(it0 != userInput.end()){
            changeFile0 = true;
            map<wstring, wstring> values = params[i].second;
            map<wstring, wstring>::iterator it1 = values.find(L"printerName");
            it1->second = it0->second;
            lineListSizes = Files::ConfigSrv::InsertParamsInSect(lineListSizes, values, sectName);
        }
    }
    if(changeFile0){
        lineListSizes.push_back(L""); // for work in acad (???)
        result = Files::Files::CreateTextFile(iniFileNameSizes, lineListSizes);
    }

    if(result){
        wstring sectName = L"main";
        list<wstring> lineListMain = Files::Files::GetFileLinesList(iniFileNameMain);
        map<wstring, wstring> values = Files::ConfigSrv::GetSectParams(iniFileNameMain, sectName);

        map<wstring, wstring>::iterator it;
        it = values.find(L"_acad");
        if(it != values.end()){
            it->second = this->result.acad;
        }
        it = values.find(L"_profile");
        if(it != values.end()){
            it->second = this->result.profile;
        }
        it = values.find(L"_pdfPlotter");
        if(it != values.end()){
            it->second = ui->comboBoxPdfPlotter->currentText().toStdWString();
        }

        lineListMain = Files::ConfigSrv::InsertParamsInSect(lineListMain, values, sectName);
        lineListMain.push_back(L""); // for work in acad (???)
        result = Files::Files::CreateTextFile(iniFileNameMain, lineListMain);
    }

    return result;


}
