#ifndef FILESACD_H
#define FILESACD_H

#include <list>
#include "mainwindow.h"

using namespace std;

namespace Files
{
    class FilesACD{
    public:
        FilesACD();
        ~FilesACD();

        static bool CreateSetupScriptAcd(bool isSetup, wstring fileName, bool isSilent);
        static bool CreatePlottersDescScript(wstring fileName, wstring appPath);
        static bool CreatePlotScript(wstring scriptFileName,
                                     wstring appPath,
                                     wstring plotStyle,
                                     int copies,
                                     bool pdfPlot,
                                     wstring outPdfPath);
    };
}

#endif // FILESACD_H
