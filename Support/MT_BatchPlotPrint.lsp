; Copyrighted by Maks Tulin
; 2019
;
; Print all drawing frames in file.
; (check that the frame is unique for each space)
;

(defun MT-getPlotAreaList (valuesList framesLayer / keyName item file values value collect result count ent item nn rec)
; return props (space, name, insertion point and scale), blocks (frames) in the drawing on layer "X"

	(setq collect (ssget "_X" (list (cons 0 "INSERT") (cons 8  framesLayer))))
	(setq count 0)
	(if collect
		(while (setq ss (ssname collect count))
			(progn
				(setq ent (entget ss))
				(setq name (cdr (assoc 2 ent)))
				(foreach nn valuesList 
					(setq value (cadr nn))
					(setq blockName
						(cdr
							(assoc "blockName" value)
						)
					)
					(if (= name blockName)
						(progn
							(setq  rec (tblsearch "BLOCK" name))
							(if (and
							    	(= (cdr (assoc 70 rec)) 0)
								(= (cdr (assoc 41 ent))
									(cdr (assoc 42 ent))
								)
								(if (= (cdr (assoc 410 ent)) "Model")
									(progn
										(setvar "ctab" "Model")
										(setq ang
											(atan
												(/
													(cadr (getvar "UCSXDIR"))
													(car (getvar "UCSXDIR"))
												)
											)
										)
										(<=
											(abs
												(-
													(cdr (assoc 50 ent))
													ang
												)
											)
											0.0001
										)
										
									)
									(= (cdr (assoc 50 ent)) 0))
								)
								(progn
									(setq item
										(list 
											(cons "NAM" (car nn))
											(cons "TAB" (cdr (assoc 410 ent)))
											(cons "BLOCK" (cdr (assoc 2 ent)))
											(cons "INS" (cdr (assoc 10 ent)))
											(cons "SCL" (cdr (assoc 41 ent)))
										)
									)
									(setq result
										(append result (list item))
									)
								)
							)
						)
					)
				)
				(setq count (1+ count))
			)
		)
	)
	result
)

(defun MT-checkPlotAreaList (plotAreaList / tabs tabsList ent item result counter x)
; chek: one space - one block only !!!

	(setq result T)
	
	(setq tabs (cdr (assoc -1  (dictsearch (namedobjdict) "ACAD_LAYOUT"))))
	(setq tabsList '())
	(setq ent (reverse (dictnext tabs T)))
	(while ent
		(setq item 
			(cdr (assoc 1 ent))
		)
		(setq tabsList (cons item tabsList))
		(setq ent 
			(reverse (dictnext tabs nil))
		)
	) ; list of tabs
	
	(foreach x tabsList
		(progn
			(setq counter 0)
			(foreach y plotAreaList
				(progn
					(setq item 
						(cdr (assoc  "TAB" y))
					)
					(if (= item x)
						(setq counter (1+ counter))
					)
					(if (> counter 1)
						(setq result nil)
					)
				)
			)
		)
    )
	
	result
)

(defun MT-getPaperForPrint (paperSizes areaW areaH toleranceMax / tolerance paperTolerance width height x
							isOversize effectWidth effectHeight tmpDW tmpDH paperName papers tmp result)
; return list of paper parametrs: ((canonicalname . localname) (width height) (effectWidth effectHeight) isOwersize isFit)

	(setq papers (list nil nil nil)); (= > <)
	(setq tolerance 1.0) ; 1.0 !!
	(setq paperTolerance 1.0) ; MM!! from INI !!!
	(setq toleranceMax (* toleranceMax 2))

	(while (<= tolerance toleranceMax)
		(progn
			(foreach x paperSizes
				(setq isOversize (cadddr x))
				(setq width (caadr x))
				(setq height (cadadr x))
				(setq effectWidth (caaddr x))
				(setq effectHeight (car (cdaddr x)))
				(setq paperName (cdar x))
				
				(setq tmpDW (- effectWidth areaW))
				(setq tmpDH (- effectHeight areaH))
				
				(if (and
						isOversize
						(= (car papers) nil)
				      		(> (+ areaW paperTolerance) width (- areaW paperTolerance))
						(> (+ areaH paperTolerance) height (- areaH paperTolerance))
					) ; check paper size
					(progn
						(setq tmp (append x (list T))) ; fit
						(setq papers
							(list tmp (cdr papers))
						); if plot area == paper size and is oversize
					)
					(if (and
							(= (cadr papres) nil)
							(> tolerance tmpDW 0.0)
							(> tolerance tmpDH 0.0)
						)
						(progn
							(setq tmp (append x (list nil))) ; not fit
							(setq papers
								(list 
									(car papers)
									tmp 
									(caddr papers)
								)
							)
						); if plot area < papersize - select min papersize
						(if (and
								(= (caddr papers) nil)
								(< (abs tmpDW) tolerance)
								(< (abs tmpDH) tolerance)
							)
							(progn
								(setq tmp (append x (list T))) ; fit
								(setq papers
									(list
										(car papers)
										(cadr papers)
										tmp
									)
								)
							); if plot area > papersize - seleect max papersize
						)
					)
				)
			)
			(if
				(or
					(car papers)
					(cadr papers)
					(caddr papers)
				) ; �������� ������� !!!!
				(setq tolerance (1+ toleranceMax))
				(setq tolerance (1+ tolerance))
			)
		)
	)
	(while (and
			papers
			(= (setq result (car papers)) nil)
		)
		(setq papers (cdr papers))
	) ; select paper
	result
)

(defun MT-printFrames (path plotTable copies plotToFile outPath / aVer keyName sectName cfgFile framesLayer pdfPlotter papersizesIniFile framesList valuesList item doc layouts logFile
						logPath activeTab plotDeviceList plotDevicePropertyList paperSizes name insertPoint scale valuesListItem tab orient areaW areaH areaWReal areaHReal  uniqueFrame 
						printerName printerDesc printersDescList paperSizes x isOversize width height paperName msg paper isFit canonicalPaperName num denom isPlot outFileName cnt oldVal isValChanged)
		     	
		     	
; main function
; call: (MT-printFrames path plotTable 1 1 nil)
;       (MT-printFrames path "" 1 1 "")

	(vl-load-com)
	(setq doc (vla-get-ActiveDocument (vlax-get-acad-object)))
	(setq layouts (vla-get-Layouts doc))
	; (setq modelSpace (vla-get-ModelSpace doc))
	
	(setq logPath (vl-string-right-trim "\\Support"  path))
	(setq logFile 
		(strcat 
			logPath 
			"\\" 
			"MT_batchplothistory " 
			(getenv "computername")
			" "
			(getenv "username")
			".log"
		)
	)
	(setq msg 
		(strcat "- " (getvar "DWGPREFIX") (getvar "DWGNAME") " :")
	)
	(MT-writeLog logFile msg)
	
	(setq cfgFile (strcat path "\\" "MT_batchplot.ini"))
	(setq sectName "main")
	(setq keyName "_commonLay")
	(setq framesLayer (MT-getValue cfgFile sectName keyName))
	(setq keyName "_pdfPlotter")
	(setq pdfPlotter (MT-getValue cfgFile sectName keyName))
	
	(setq sectName "plot")
	(setq keyName "_uniqueFrame")
	(setq uniqueFrame (atoi (MT-getValue cfgFile sectName keyName)))
	(if (or (/= uniqueFrame 0) (= uniqueFrame nil))
		(setq uniqueFrame 1)
	)
	(setq keyName "_toletanceMax") ; 7.0
	(setq toleranceMax (atof (MT-getValue cfgFile sectName keyName)))

	(setq papersizesIniFile (strcat path "\\" "MT_papersizes.ini"))
	(setq framesList (MT-getSectList papersizesIniFile))
	
	(foreach x framesList
		(setq item 
			(list
				x
				(MT-getConfigList papersizesIniFile x)
			)
		)
		(setq valuesList 
			(append valuesList 
				(list item)
			)
		)
	)
	(setq plotAreaList (MT-getPlotAreaList valuesList framesLayer))
	(if (= nil plotAreaList)
		(progn
			(setq msg "-- Error! Objects for plotting not found!")
			(princ msg)
			(MT-writeLog logFile msg)
			(exit)
		) ; Error -> write log -> exit
	)

	(if (= uniqueFrame 1)
		(setq enablePrinting
			(MT-checkPlotAreaList plotAreaList)
		) ; if num blocks > 1 in space - error!
		(setq enablePrinting T)
	)

	(setq aVer 
		(atof 
			(substr (getvar "ACADVER") 1 4)
		)
	)
	(if (>= aVer 21.0)
		(progn
			(setq oldVal (getvar "PDFSHX"))
			(setvar "PDFSHX" 0)
			(setq isValChanged T)
		)
	)

	(if enablePrinting
		(progn
			(setvar "WIPEOUTFRAME" 0) ; hide frames 
			(if (= plotTable "")
				(setq plotTable "acad.ctb")
			)
			(if (= plotToFile 1)
				(setq copies 1)
			)
			(if (or (= outPath "") (= outPath nil))
				(setq outFileName
					(vl-string-right-trim
						".dwg"
						(strcat (getvar "DWGPREFIX") (getvar "DWGNAME"))
					)
				)
				(setq outFileName
					(vl-string-right-trim
						".dwg"
						(strcat outPath "\\" (getvar "DWGNAME"))
					)
				)
			) ; for pdf plot

			(foreach x plotAreaList
				(setq tab (cdr (assoc "TAB" x)))
				(setq name (cdr (assoc "NAM" x)))
				(setq insertPoint
					(list
						(car (cdr (assoc "INS" x)))
						(cadr (cdr (assoc "INS" x)))
					)
				) ; to plot area
				(setq scale (cdr (assoc "SCL" x)))
				(setq valuesListItem (cadr (assoc name valueslist)))
				(setq areaW (atof (cdr (assoc "paperW" valuesListItem)))) ; to plot area
				(setq areaH (atof (cdr (assoc "paperH" valuesListItem)))) ; to plot area
				(if (= plotToFile 1)
					(setq printerName pdfPlotter)
					(setq printerName (cdr (assoc "printerName" valuesListItem)))
				)

				(if (> areaW areaH)
					(setq orient "_Landscape")
					(setq orient "_Portrait")
				) ; for command "_-plot"

			  	(if
					(=
						(setq printerDesc 
							(assoc
								(cons "device" printerName)
								printersDescList
							)
						)
						nil
					)
					(progn
						(setq printerDesc 
							(MT-getPlotterDesc 
								(strcat path "\\Printers") 
								printerName
							)
						)
						(if (/= printerDesc nil)
							(if (= printersDescList nil)
								(setq printersDescList (list printerDesc))
								(setq printersDescList (list printersDescList printerDesc))
							)
						)
					)
				)
				(if (= printerDesc nil)
					(progn
						(setq msg (strcat "-- tab \"" tab "\" - Error! Undefined printer."))
						(princ msg)
						(MT-writeLog logFile msg)	
					)
					(progn
						(setq paperSizes
							(cadr 
								(assoc "paperSizes" printerDesc)
							)
						)
						(setq paper (MT-getPaperForPrint paperSizes areaW areaH toleranceMax)) ; select paper
						(if (= paper nil)
							(progn
								(setq paper (MT-getPaperForPrint paperSizes areaH areaW toleranceMax))
								(if paper
									(setq rotate T)
								)
							)
							(setq rotate nil)
						)
						(if (= paper nil)
							(progn
								(setq msg (strcat "-- tab \"" tab "\" - Error! Paper size are not supported."))
								(princ msg)
								(MT-writeLog logFile msg)
;;;								(exit)
							) ; Error -> write log -> exit (or next tab)??
							(progn
								(setq areaWReal (* areaW scale))
								(setq areaHReal (* areaH scale))
								(setq isFit (car (cddddr paper)))
								(setq isOversize (cadddr paper))
								(setq width (caadr paper))
								(setq height (cadadr paper))
								(setq effectWidth (caaddr paper))
								(setq effectHeight (car (cdaddr paper)))
								(setq paperName (cdar paper))
								(setq canonicalPaperName (caar paper))

								(vla-put-ActiveLayout doc (vla-item layouts tab))
								(setq activeTab (vla-get-ActiveLayout doc))

								(vla-put-ConfigName activeTab printerName) ; set plot device
								(vla-put-CanonicalMediaName activeTab canonicalPaperName) ; set paper size
								(vla-put-PaperUnits activeTab acMillimeters) ; set paper units - MM !!!

								(setq pointFrame0 (vlax-make-safearray vlax-vbDouble '(0 . 1)))
								(setq pointFrame1 (vlax-make-safearray vlax-vbDouble '(0 . 1)))
								(vlax-safearray-fill pointFrame0 insertPoint)
								(vlax-safearray-fill
									pointFrame1
									(list
										(+ (car insertPoint) areaWReal)
										(+ (cadr insertPoint) areaHReal)
									)
								)
								
								(vla-SetWindowToPlot 
									activeTab 
									pointFrame0 
									pointFrame1
								) ; set window to plot
								(vla-put-PlotType activeTab acWindow) ; set PlotType property to window
								(if(/= scale 1.0)
									(progn
										(vla-put-UseStandardScale activeTab :vlax-false)
										(vla-put-StandardScale activeTab acVpCustomScale)
										(setq num 1.0)
										(if (= isFit nil)
											(setq denom scale)
											(if (= rotate nil)
												(setq denom
													(max
														(/ areaWReal effectWidth)
														(/ areaHReal effectHeight)
													)
												)
												(setq denom
													(max
														(/ areaHReal effectWidth)
														(/ areaWReal effectHeight)
													)
												)
											)
										)
										(vla-SetCustomScale activeTab num denom) ; custom scale
									)
									(progn
										(vla-put-UseStandardScale activeTab :vlax-true)
										(if (= isFit nil)
											(vla-put-StandardScale activeTab ac1_1) ; plot area < effect area
											(vla-put-StandardScale activeTab acScaleToFit) ; plot area >= effect area
										)
									)
									
								) ; set scale
								(setq origin (vlax-make-safearray vlax-vbDouble '(0 . 1)))
								(vlax-safearray-fill origin (list 0.0 0.0))
								(vla-put-PlotOrigin activeTab origin) ; set plot origin
								(vla-put-CenterPlot activeTab :vlax-true) ; set plot center
								(if (= rotate nil)
									(vla-put-PlotRotation activeTab ac0degrees)
									(vla-put-PlotRotation activeTab ac90degrees)
								) ; set rotation (if rotate = T ac90degrees)
								(vla-put-PlotHidden activeTab :vlax-false) ; hide paperspace objects (no)
								(vla-put-PlotViewportBorders activeTab :vlax-false) ; set viewport plot behavior
								(vla-put-PlotViewportsFirst activeTab :vlax-true)
								(vla-put-PlotWithLineweights activeTab :vlax-true) ; set lineweight behavior
								(vla-put-ScaleLineweights activeTab :vlax-false) ; !!!
								(vla-put-PlotWithPlotStyles activeTab :vlax-true) ; set plot styles behavior
								(vla-put-ShowPlotStyles activeTab :vlax-true)
								(if (= (getvar "PSTYLEMODE") 0)
									(vla-put-StyleSheet activeTab "acad.stb") ; fix it !!!
									(vla-put-StyleSheet activeTab plotTable)
								)

								(setq msg (strcat "-- \"" tab "\" - Success."))
								
								(setq cnt 0)
								(while (< cnt copies)
									(progn
										(if (= plotToFile 1)
											(setq isPlot
												(vla-PlotToFile
													(vla-get-Plot doc)
													(strcat outFileName " - " tab ".pdf")
												) ; file is rewriting!!
											)
											(setq isPlot (vla-PlotToDevice (vla-get-Plot doc)))
										)
										(if (= isPlot :vlax-false)
											(progn
												(setq msg (strcat "-- tab \"" tab "\" - Printing error!"))
;;;												(MT-writeLog logFile msg)
												(princ msg)
;;;												(exit)
											)
										) ; in model tab - exeption????
										(setq cnt (1+ cnt))
									)
								)
								(MT-writeLog logFile msg)
							)
						)
					)
				)
			)
		)
		(progn
			(setq msg "-- Error! Too many blocks in one space.")
			(princ msg)
			(MT-writeLog logFile msg)
		)
	)
	(if isValChanged
		(setvar "PDFSHX" oldVal)
	)
	(princ "Done.")
)
