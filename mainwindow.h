#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"
#include <QMainWindow>
#include <QStringListModel>
#include <QString>
#include <QList>
#include <QDir>
#include <QKeyEvent>
#include <vector>
#include <string>
#include "Reg.h"
#include "Acad.h"
#include "thread.h"
#include "dialogprefs.h"

namespace Ui {

struct ACDParamsState{
    bool isSecureModeChanged;
    bool isSysvarsChanged;
};

struct ACDSysvars{
    vector<pair<wstring, unsigned long>> sysvarsDefaultList;
    vector<pair<wstring, unsigned long>> sysvarsList;
    vector<pair<wstring, unsigned long>> sysvarsListBakup;
};

class MainWindow;

}

class MainWindow :  public QMainWindow, public Ui_MainWindow
{
    Q_OBJECT

public:
    Reg::RegFunc *registry;
    vector<Acad::AcadApp>  acadList;
    wstring path;
    wstring pdfPath;
    wstring appPath;
    wstring acadName;
    wstring profileName;
    wstring pdfPlotterInit;
    int timeOut;
    int timeMult;
    bool createSubDirsForPdf;
    bool isCalling;
    bool isSilent;
    bool enabled;

    explicit MainWindow(QWidget *parent = 0, bool isCalling = false);
    ~MainWindow();

    void SetInitalState();
    void SetFileListData();
    void SetModelFileListOnStart();

protected:
    virtual void keyPressEvent(QKeyEvent *event);

signals:
    void workCanceled();

public slots:
    void SetPrefs(Ui::State value);
    void SetValue(int value);
    void AppStop(int value);
    void AppReport(int value);
    void ItemReport(int value);

private slots:
    void on_fileList_activated();
    void on_pushButtonFile_clicked();
    void on_pushButtonPdfPath_clicked();
    void on_printButton_clicked();
    void PrintButtonEnabled();
    void SlotRemoveAllRecords();
    void SlotRemoveRecords();
    void SlotCustomMenuRequested(QPoint pos);
    void SlotExit();
    void SlotCheckDir();
    void on_actionAbout_triggered();
    void on_actionAboutQT_triggered();
    void on_actionPreferences_triggered(bool onStart = false);
    void on_actionHistory_triggered();

private:
    Ui::MainWindow *ui;
    QStringListModel *modelFileList;
    QStringListModel *modelPlotStylesList;
    Acad::AcadApp *acd;
    Ui::Thread *threadObject;
    QStringList fileList;
    QStringList fileSelectedList;
    wstring lastDwgPath;
    wstring lastPdfPath;
    wstring printersDescPath;
    wstring currentFile;
    wstring taskLogFile;
    std::vector<std::pair<std::wstring, bool>> reportList;
    Ui::State currentState;
    Ui::ACDParamsState paramsState;
    Ui::ACDSysvars acadSysvars;
    vector<pair<wstring, wstring>> currentProfilePropertyesList;
    int count;
    bool printersDescExist;
    bool printEnable;
    bool appRuning;


    QStringList GetFileList(QString dirPath, QString mask);
    QStringList GetProfilesList(Acad::AcadApp  *app);
    QStringList GetPlotStylesList(Acad::AcadApp  *app, wstring profileName);
    void SetModelFileList(QStringList newFileList, bool save);
    void SetComboBoxPlotStylesListData();
    void SetACDParams(wstring profile,
                      vector<pair<wstring, unsigned long>> sysvarsList,
                      vector<pair<wstring, wstring> > sysvarsStrList,
                      bool isBefore);
    void PrintInACD(Ui::State currentState);
    void StartACDInThread(QStringList fileNameList,
                            QString scriptFileName,
                            QString profile,
                            QString path,
                            QString text,
                            int timeOut,
                            bool silent);
    void StartAppInThread(QStringList fileNameList,
                            QString scriptFileName,
                            QString profile,
                            QString path,
                            QString text,
                            int timeOut); // ??
    bool CompareState(Ui::State state1, Ui::State state2);
    bool CheckOutDir(wstring dir);
    void CreatePrintersDesc();
    void ClearPrintersDescDir();
    void ReportError();

};

#endif // MAINWINDOW_H
