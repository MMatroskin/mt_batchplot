#include "comboboxdelegate.h"
#include <QComboBox>

namespace Ui{

    ComboBoxDelegate::ComboBoxDelegate(QObject *parent) : QStyledItemDelegate(parent){

    }

//    ComboBoxDelegate::~ComboBoxDelegate(){

//    }

    QWidget *ComboBoxDelegate::createEditor(QWidget *parent,
                                            const QStyleOptionViewItem &/* option */,
                                            const QModelIndex &/* index */) const {


        QComboBox *editor = new QComboBox(parent);
//        editor->setFrame(false);
//        editor->insertItems(0, ZTypes().getData()); // change!!

        editor->addItems(printers);

        return editor;
    }

    void ComboBoxDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {

        QComboBox *comboBox = static_cast<QComboBox*>(editor);
//        QString txt = index.model()->data(index, Qt::DisplayRole).toString();
//        comboBox->setCurrentIndex(comboBox->findText(txt));

        QStringListModel *modelPrinters = new QStringListModel(printers);
        QString currentText;

        int idx0 = index.model()->data(index, Qt::EditRole).toInt();
        currentText = printers[idx0];
        //
//        int idx1 = index.row();
//        QStringList valueList = printersCurrent;
//        valueList.replace(idx1, currentText);

        comboBox->setModel(modelPrinters);
        int cbIndex = comboBox->findText(currentText);
//        int cbIndex = comboBox->findText(plotterCurrent);
        if (cbIndex >= 0){
            comboBox->setCurrentIndex(cbIndex);
        }else{
            comboBox->setCurrentIndex(0);
        }
//        comboBox->setCurrentIndex(cbIndex);
    }

//    void ComboBoxDelegate::SetEditorData(QWidget *editor, const QModelIndex &index) const{

//        QComboBox *comboBox = static_cast<QComboBox*>(editor);
//        QString value = currentValue;
//        if(value.isEmpty()){
//            value = index.model()->data(index, Qt::DisplayRole).toString();
//        }
//        comboBox->setModel(this->modelPrinters);
//        int idx = comboBox->findText(value);
//        if (idx == -1){
//            idx = 0;
//        } // fix it later
//        comboBox->setCurrentIndex(idx);
//    }

    void ComboBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                      const QModelIndex &index) const {

       QComboBox *comboBox = static_cast<QComboBox*>(editor);
//       QString txt = comboBox->currentText();
//       model->setData(index, txt, Qt::EditRole);

       model->setData(index, comboBox->currentIndex(), Qt::EditRole);

   }

    void ComboBoxDelegate::updateEditorGeometry(QWidget *editor,
                                                const QStyleOptionViewItem &option,
                                                const QModelIndex &/* index */) const{

        editor->setGeometry(option.rect);
    }

}
