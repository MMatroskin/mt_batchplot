#include "mainwindow.h"
#include <QApplication>
#include <QMessageBox>
#include <QStyleFactory>
#include <fstream>
#include "acadsrv.h"
#include "configSrv.h"
#include "files.h"

// ошибка линковки с2017:
// QT необходимо указывать внешние либы, для msvs - #pragma comment
// для MinGW - LIBS += /путь_до_либы/libastral.a  в .pro-файле (стат.линк.)
// либо - LIBS += -L./путь_до_либы/ -lastral  в .pro-файле (дин.линк.)

#pragma comment(lib, "Advapi32.lib")
#pragma comment(lib, "User32.lib")

#define TIME_OUT 240000// time out 4 min
#define TIME_MULT 2

using namespace std;

int main(int argc, char *argv[])
{
    bool isCalling = false;
    if (argc > 1){
        isCalling = true;
    }

    wstring workDir = L"";
    wstring pdfDir = L"";
    bool createSubDirsForPdf = false; // ??? - later
    vector<Acad::AcadApp> acadList = Acad::AcadSrv::GetInstalledAcad();

    if (acadList.empty()){
        QString message = "AutoCAD is not found on this computer";
        QMessageBox msgBox;
        msgBox.setText(message);
        msgBox.setWindowTitle("Error!");

        msgBox.exec();

        return 0;
    }

    // get current path
    wstring currentPath;
    WCHAR buffer[MAX_PATH];
    DWORD r = GetModuleFileName(NULL, buffer, sizeof(buffer) / sizeof(buffer[0]));
    if(r > 0){
        currentPath = buffer;
        size_t p = currentPath.find_last_of(L"\\");
        currentPath = currentPath.substr(0, p);
    }else{
        wchar_t path[MAX_PATH];
        GetCurrentDirectory(sizeof(path), path);
        currentPath = path; // path of parent app !!
    }

    // get current Acad and Profile (from INI)
    wstring currentAcad = L"";
    wstring currentProfile = L"";
    wstring pdfPlotter = L"";
    int timeOut = TIME_OUT;
    int timeMult = TIME_MULT;
    wstring appPath = currentPath;
    wstring fileName = appPath + L"\\Support\\MT_batchplot.ini";
    wstring sectName = L"main";
    map<wstring, wstring> params = Files::ConfigSrv::GetSectParams(fileName, sectName);

    map<wstring, wstring>::iterator it0 = params.find(L"_acad");
    if(it0 != params.end()){
        currentAcad = it0->second;
    }
    map<wstring, wstring>::iterator it1 = params.find(L"_profile");
    if(it1 != params.end()){
        currentProfile = it1->second;
    }
    map<wstring, wstring>::iterator it2 = params.find(L"_pdfPlotter");
    if(it2 != params.end()){
        pdfPlotter = it2->second;
    }
    map<wstring, wstring>::iterator it3 = params.find(L"_timeOut");
    if(it3 != params.end()){
        timeOut = std::stoi(it3->second);
    }
    map<wstring, wstring>::iterator it4 = params.find(L"_timeMult");
    if(it4 != params.end()){
        timeMult = std::stoi(it4->second);
    }

    // search current Acad and current Profile
    wstring acad = L"";
    wstring profile = L"";
    if(currentAcad != L"" && currentProfile != L""){
        for(size_t i = 0; i < acadList.size(); i++){
            wstring itemName = acadList[i].GetProductName();
            if(itemName == currentAcad){
                acad = itemName;
                vector<wstring> profilesList = acadList[i].GetAcadProfiles();
                for(size_t j = 0; j < profilesList.size(); j++){
                    if(profilesList[j] == currentProfile){
                        profile = profilesList[j];
                        break;
                    }
                }
                break;
            }
        }
    }

    // rewrite file ACAD.LSP (for Acad start)
    wstring lspFileName = appPath + L"\\Support\\acad.lsp";
    list<wstring> lineList = Files::Files::GetFileLinesList(lspFileName);
    if(lineList.back() == L""){
        lineList.pop_back();
    }
    bool rwAcdLsp = Files::Files::CreateTextFile(lspFileName, lineList);

    // get working dir for  DWG files
    if(isCalling){

        // convert
        char* szMbStr = argv[1];        // src string
        wchar_t awcBuffer[8081];        // dst string

        // Windows Default Codepage -> UTF-16
        int nCnt = MultiByteToWideChar(CP_ACP, 0, szMbStr, -1, awcBuffer, sizeof(awcBuffer));
        if (nCnt == 0){
            QString message = "Invalid input!";
            QMessageBox msgBox;
            msgBox.setText(message);
            msgBox.setWindowTitle("Error!");

            msgBox.exec();

            exit(1);
        }
        wstring paramStr = awcBuffer;

        wstring oldSep = L"$"; // spaces
        wstring newSep = L" ";

        if(!paramStr.empty()){
            paramStr = Files::Files::ChangeSubstr(paramStr, oldSep, newSep);
        }

        wstring rootDir = currentPath;
        size_t pos = rootDir.find_last_of(L"\\");
        rootDir = rootDir.substr(0 , pos);
        list<wstring> iniFiles = Files::Files::SearchFiles(rootDir, L"MT_Projects.ini", false); // to parent app??
        if(iniFiles.size() == 0){
            QString message = "Configuratin file not found!";
            QMessageBox msgBox;
            msgBox.setText(message);
            msgBox.setWindowTitle("Error!");

            msgBox.exec();

            exit(1);
        }

        wstring iniFileName = iniFiles.front();

        map<wstring, wstring> paramsDirs = Files::ConfigSrv::GetSectParams(iniFileName, L"partDirs");
        map<wstring, wstring> paramsCmd;
        wstring sep0 = L"#"; // params
        wstring sep1 = L"="; // keys/values
        size_t prev = 0;
        size_t next;
        size_t delta = 1;
        size_t posCurrrent;
        wstring key;
        wstring value;
        bool cont;

        // get params from argv
        do{
            cont = false;
            wstring tmp = paramStr.substr(prev);
            if((next = paramStr.find(sep0, prev)) != wstring::npos ){
                tmp = paramStr.substr(prev, next - prev );
                cont = true;
            }
            if((posCurrrent = tmp.find(sep1, 0)) != wstring::npos ){
                key = tmp.substr(0, posCurrrent);
                value = tmp.substr(posCurrrent + 1);
                paramsCmd.insert(pair<wstring, wstring>(key, value));
            }
            prev = next + delta;
        }while(cont);

        // set work dir
        wstring subdir;
        map<wstring, wstring>::iterator it5;
        it5 = paramsDirs.find(L"_drawingsdir");
        if(it5 != paramsDirs.end()){
            subdir = it5->second;
        }
        map<wstring, wstring>::iterator it6;
        it6 = paramsCmd.find(L"path");
        if(it6 != paramsCmd.end()){
            workDir = it6->second;
            workDir = Files::Files::ChangeSubstr(workDir,L"\\\\",L"\\");
            workDir = workDir + L"\\" + subdir;
        }
        map<wstring, wstring>::iterator it7;
        it7 = params.find(L"pdfPath"); // main PDF dir or target dir ???
        if(it7 != params.end()){
            if(it7->second != L""){
                pdfDir = it7->second;
                pdfDir = Files::Files::ChangeSubstr(pdfDir,L"\\\\",L"\\");
            }
        }
        createSubDirsForPdf = false; // if not target dir - true??
    }

    QApplication app(argc, argv); /// !!
    app.setStyle(QStyleFactory::create("Fusion"));

    if (acadList.empty()){

        QString message = "ACAD application not found!";
        QMessageBox msgBox;
        msgBox.setText(message);
        msgBox.setWindowTitle("Error!");

        msgBox.exec();

    }else{
        MainWindow w(0, isCalling);

        w.appPath = currentPath;
        w.path = workDir; // for files
        w.pdfPath = pdfDir;
        w.acadList = acadList;
        w.acadName = acad;
        w.profileName = profile;
        w.pdfPlotterInit = pdfPlotter;
        w.createSubDirsForPdf = createSubDirsForPdf;
        w.timeOut = timeOut;
        w.timeMult = timeMult;
        w.isSilent = true; // in test - false, in prod - true

        w.SetInitalState();

        if(w.enabled){
            w.show();
            return app.exec();
        }else{
            return 0;
        }
    }
}
