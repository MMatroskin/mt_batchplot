#include "Reg.h"
#include <algorithm>
#include <climits>

using namespace std;

//#define MAX_KEY_LENGTH 260 // ANSI
#define MAX_KEY_LENGTH 16383 // UNICODE

namespace Reg{

	RegFunc::RegFunc(){}

    RegFunc::~RegFunc(){}
	
    wstring RegFunc::GetParametr(HKEY key, wstring subKey, wstring name){

		HKEY rKey;
        wstring result;
		wstring subKeyTmp(subKey.begin(), subKey.end());
		wstring nameTmp(name.begin(), name.end());

        long dwRegOpenKey = RegOpenKeyEx(key, subKeyTmp.c_str(), 0, KEY_READ, &rKey);
        if (dwRegOpenKey == ERROR_SUCCESS){
			wchar_t* buf[MAX_KEY_LENGTH];
            DWORD bufSize = MAX_KEY_LENGTH;
			long currentKeyValue = RegGetValue(key, subKeyTmp.c_str(), nameTmp.c_str(), RRF_RT_REG_SZ, 0, buf, &bufSize);
			if (currentKeyValue == ERROR_SUCCESS){
				wstring wsTmp;
				wsTmp = (wchar_t*)buf;
				string sTmp(wsTmp.begin(), wsTmp.end());
                result = wsTmp;
			}
//            RegCloseKey(rKey);
		}
        RegCloseKey(rKey);

		return result; // in fail return empty string
	}

    unsigned long RegFunc::GetDwParametr(HKEY key, wstring subKey, wstring name){

        DWORD result = 0;
        HKEY rKey;
        wstring subKeyTmp(subKey.begin(), subKey.end());
        wstring nameTmp(name.begin(), name.end());

        long dwRegOpenKey = RegOpenKeyEx(key, subKeyTmp.c_str(), 0, KEY_READ, &rKey);
        if (dwRegOpenKey == ERROR_SUCCESS){
            DWORD buf;
            DWORD bufSize = sizeof(DWORD);
            long currentKeyValue = RegGetValue(key, subKeyTmp.c_str(), nameTmp.c_str(), RRF_RT_REG_DWORD, 0, &buf, &bufSize);
            if (currentKeyValue == ERROR_SUCCESS){
                result = buf;
            }
            else {
                result = ULONG_MAX;
            }
//            RegCloseKey(rKey);
        }
        RegCloseKey(rKey);

        return result; // в случае фэйла возвращаем 0 ???
    }

    // list of subkeys
    vector<wstring> RegFunc::GetSubKeys(HKEY key, wstring subKey){

        vector<wstring> subKeysVec; // строки вида "R20.1"
		HKEY rKey;
        wstring subKeyTmp(subKey.begin(), subKey.end()); // del

        long dwRegOpenKey0 = RegOpenKeyEx(key, subKeyTmp.c_str(), 0, KEY_READ, &rKey);
        if (dwRegOpenKey0 == ERROR_SUCCESS){
			DWORD lim = 1;
			for (DWORD i = 0; i < lim; i++){
				DWORD length = MAX_KEY_LENGTH;
                wchar_t* cName = new wchar_t[length];
				long regEnumKeys = RegEnumKeyEx(rKey, i, cName, &length, NULL, NULL, NULL, NULL); // имя  i-го подраздела
				if (regEnumKeys == ERROR_SUCCESS){
                    // convert 2 std::string
                    wstring wsName = (wchar_t*)cName;
                    string sName(wsName.begin(), wsName.end()); // кракозябры
                    //subKeysVec.push_back(sName);
                    subKeysVec.push_back(wsName);
					lim++; // для продолжения цикла
				}

			}
//            RegCloseKey(rKey);
		}
		RegCloseKey(rKey);

		return subKeysVec;
	}

    bool RegFunc::SetParametr(HKEY rKey, wstring subKey, wstring name, wstring value){

        bool result = false;
        wstring subKeyTmp(subKey.begin(), subKey.end());
        wstring nameTmp(name.begin(), name.end());
        wstring valueTmp(value.begin(), value.end());
        HKEY hKey;

//        DWORD dT1 = REG_SZ;     // 1
//        DWORD dT2 = REG_DWORD;  // 4

        long dwRegOpenKey = RegCreateKeyEx(rKey, subKeyTmp.c_str(), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hKey, NULL);
        if (dwRegOpenKey == ERROR_SUCCESS){
            DWORD dwRegSetValKey = RegSetValueEx(hKey, nameTmp.c_str(), 0, REG_SZ, (const BYTE*)valueTmp.c_str(), sizeof(wchar_t)*(wcslen(valueTmp.c_str())+1));
            if(dwRegSetValKey == ERROR_SUCCESS){
                result = true;
            }
            RegFlushKey(hKey);
            RegCloseKey(hKey);
        }

        return result;
    }

    bool RegFunc::SetParametr(HKEY rKey, wstring subKey, wstring name, unsigned long value){

        bool result = false;
        wstring subKeyTmp(subKey.begin(), subKey.end());
        wstring nameTmp(name.begin(), name.end());
        HKEY hKey;

//        DWORD dT1 = REG_SZ;     // 1
//        DWORD dT2 = REG_DWORD;  // 4

        long dwRegOpenKey = RegCreateKeyEx(rKey, subKeyTmp.c_str(), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hKey, NULL);
        if (dwRegOpenKey == ERROR_SUCCESS){
            DWORD dwRegSetValKey = RegSetValueEx(hKey, nameTmp.c_str(), 0, REG_DWORD, (const BYTE*)&value, sizeof(value));
            if(dwRegSetValKey == ERROR_SUCCESS){
                result = true;
            }
            RegFlushKey(hKey);
            RegCloseKey(hKey);
        }

        return result;
    }

    bool RegFunc::CreateItem(HKEY rKey, wstring subKey){

        bool result = false;
        wstring subKeyTmp(subKey.begin(), subKey.end());
        HKEY hKey;

//        DWORD dT1 = REG_SZ;     // 1
//        DWORD dT2 = REG_DWORD;  // 4

        long dwRegKey = RegCreateKeyEx(rKey, subKeyTmp.c_str(), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hKey, NULL); // acsess??
        if (dwRegKey == ERROR_SUCCESS){
            result = true;
            RegFlushKey(hKey);
            RegCloseKey(hKey);

        }

        return result;
    }

}
