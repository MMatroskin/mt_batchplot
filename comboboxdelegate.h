#ifndef COMBOBOXDELEGATE_H
#define COMBOBOXDELEGATE_H

#include <QStyledItemDelegate>
#include <QStringListModel>
#include <QString>
#include "dialogprefs.h"

namespace Ui {

    class ComboBoxDelegate : public QStyledItemDelegate
    {
        Q_OBJECT
    public:
        ComboBoxDelegate(QObject * parent = nullptr);
//        ~ComboBoxDelegate();

        QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,const QModelIndex &index) const Q_DECL_OVERRIDE;
        void setEditorData(QWidget *editor, const QModelIndex &index) const Q_DECL_OVERRIDE;
        void setModelData(QWidget *editor, QAbstractItemModel *model,const QModelIndex &index) const Q_DECL_OVERRIDE;
        void updateEditorGeometry(QWidget *editor,const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;

        QStringList printers;
        QStringList printersCurrent;
    };

}


#endif // COMBOBOXDELEGATE_H
