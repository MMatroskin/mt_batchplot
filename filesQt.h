#ifndef FILESQT_H
#define FILESQT_H

#include <list>
#include <map>
#include <QSizeF>
#include <QString>
#include "mainwindow.h"

using namespace std;

namespace Files
{
    class FilesQt{
    public:
        FilesQt();
        ~FilesQt();

        static bool DeleteFileNow(QString fileName);
        static QString ChangeSlash(QString stringIn);
        static QString AddQuotes(QString stringIn);
        static QString AddSlash(QString stringIn);

    };
}

#endif // FILESQT_H
