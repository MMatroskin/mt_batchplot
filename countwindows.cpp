/*
 * based on
 * https://blogs.msdn.microsoft.com/oldnewthing/20140127-00/?p=1963/
*/

#include "countwindows.h"

namespace Acad{

    CountWindows::CountWindows(){
        process.handle = NULL;
        process.id = 0;
    }

    CountWindows::~CountWindows(){
    }

    BOOL CountWindows::StaticWndEnumProc(HWND handle, LPARAM lParam)
    {
        CountWindows *pThis = reinterpret_cast<CountWindows* >(lParam);
        return pThis->WndEnumProc(handle);
    }

    BOOL CountWindows::WndEnumProc(HWND handle)
    {
        DWORD id = 0;
        GetWindowThreadProcessId(handle, &id);
        process.handle = handle;
        process.id = id;
        processVec.push_back(process);
        return TRUE;
    }

    std::vector<Acad::Proc> CountWindows::CountThem()
    {
        EnumWindows(StaticWndEnumProc, reinterpret_cast<LPARAM>(this));
        return processVec;
    }

}
